# List of repostiories with custom classes for FastFrames

 * Overlap removal studies: https://gitlab.cern.ch/mdubovsk/overlap_removal_studies
 * Extraction of JES from W mass: https://gitlab.cern.ch/tdado/wmassjescustomclass
 * Top mass and width measurement: https://gitlab.cern.ch/katharin/topmasswidthfastframes
 * GN2 (FTag) SFs: https://gitlab.cern.ch/dbaronmo/myanalysis-fastframes
 * TTH contamination studies: https://gitlab.cern.ch/bruiter/analysis-tthbb/-/tree/master/fastframes/customframes/Top_Contamination?ref_type=heads
 * ttZ boosted analysis: https://gitlab.cern.ch/atlasphys-top/TopPlusX/ANA-TOPQ-2023-45/customfastframesclassttz
 * ttbb lepton+jets analysis: https://gitlab.cern.ch/ubc-atlas/ttbb/ttbbljetsframe
